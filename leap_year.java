import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.time.*;
import java.util.regex.*;

public class leap_year
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);

        System.out.println("Введите дату в формате dd.mm.yyyy");
        String date_line = scan.nextLine();

        Pattern pattern = Pattern.compile("^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.]([0-9]{4})$");
        Matcher matcher = pattern.matcher(date_line);

        if (matcher.find())
        {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
            LocalDate date = LocalDate.parse(date_line, formatter);

            if(date.isLeapYear())
            {
                System.out.println("В введенной вами дате год является високосным");
            }
            else
            {
                System.out.println("В введенной вами дате год \u001b[31mне\u001b[0m является високосным");
            }
        }
        else
        {
            System.out.println("Некорректный ввод даты");
        }
    }
}